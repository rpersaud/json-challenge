Assumptions:
-The json_doc object passed to the () function is supposed to be modified in place.
-"You can assume for this exercise that outside of primitives (integers, strings, booleans, etc), no 2 nested objects inside the json document are identical."  This implies that the only time a 'container' object (list or dict) should be visited more than once is via a cycle.

Runtime:
Processing is dominated by the recursive function identify_containers_break_cycles() that processes every element in the document.  Therefore, if there are n elements in the document, preparing for serialization is O(n).

Notes:
As noted in a comment in test_challenge.py, if a document has more than ~485 levels of nesting, the maximum recursion depth is exceeded.  For documents with deep nesting, an iterative solution may be required.
