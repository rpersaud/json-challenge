#!/usr/bin/python

class Structure:
    def __init__(self, item):
        self.item = item
        self.levels = {}
        self.findStructure(self.item, 0, {})

    def findStructure(self, element, depth, seen):
        if depth not in self.levels:
            self.levels[depth] = {}
            self.levels[depth]['dict'] = 0
            self.levels[depth]['list'] = 0
            self.levels[depth]['cycle'] = 0
            self.levels[depth]['other'] = 0
        if id(element) in seen:
            self.levels[depth]['cycle'] += 1
        else:
            if type(element) is dict or type(element) is list:
                seen[id(element)] = ""
            if type(element) is dict:
                self.levels[depth]['dict'] += 1
                for key in element.keys():
                    self.findStructure(element[key], depth + 1, seen)
            elif type(element) is list:
                self.levels[depth]['list'] += 1
                for i in range (0, len(element)):
                    self.findStructure(element[i], depth + 1, seen)
            else:
                self.levels[depth]['other'] += 1
            
    def getStructure(self):
        self.findStructure(self.item, 0, {})    

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.levels == other.levels
            #return self.__dict__ == other.__dict__
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)
