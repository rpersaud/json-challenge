#!/usr/bin/python

import json
from sets import Set

PREFIX = 'wrapper-'

class ContainerElement():
    def __init__(self, identifier, parent, alias):
        self.identifier = identifier
        self.parent = parent
        self.alias = alias

def process_container(container, parent, container_elements, cycle_set, key):
    if type(container) is dict or type(container) is list:
        if id(container) not in container_elements:
            container_element = ContainerElement(key, parent, PREFIX + str(id(container)))
            container_elements[id(container)] = container_element
            identify_containers_break_cycles(container, container_elements, cycle_set)
        else:
            cycle_set.add(id(container))
            parent[key] = container_elements[id(container)].alias
 
def identify_containers_break_cycles(json_doc, container_elements, cycle_set):
    if type(json_doc) is dict:
        for key in json_doc.keys():
            process_container(json_doc[key], json_doc, container_elements, cycle_set, key)
    elif type(json_doc) is list:
        for key in range(0, len(json_doc)):
             process_container(json_doc[key], json_doc, container_elements, cycle_set, key)

def wrap_cycle_containers(container_elements, cycle_set):
    for key in cycle_set:
        element = container_elements[key]
        wrapped_element = {element.alias : element.parent[element.identifier]}
        element.parent[element.identifier] = wrapped_element

def prepare_for_serialization(json_doc):
    cycle_set = Set([])
    container_elements = {}
    # Add root entry for possible cycles involving the root container
    container_element = ContainerElement(json_doc, None, PREFIX + 'root')
    container_elements[id(json_doc)] = container_element
    identify_containers_break_cycles(json_doc, container_elements, cycle_set)

    # Remove root entry from cycle set
    if id(json_doc) in cycle_set:
        cycle_set.remove(id(json_doc))

    wrap_cycle_containers(container_elements, cycle_set)

def get_alias_mappings(parent, identifier, json_doc, mappings):
    if type(json_doc) is dict:
        for key in json_doc.keys():
            if key.startswith(PREFIX):
                mappings[key] = json_doc[key]
                parent[identifier] = json_doc[key]
                get_alias_mappings(json_doc, key, json_doc[key], mappings)
            elif type(json_doc[key]) is dict or type(json_doc[key]) is list:
                get_alias_mappings(json_doc, key, json_doc[key], mappings)
    elif type(json_doc) is list:
        for i in range(0, len(json_doc)):
            get_alias_mappings(json_doc, i, json_doc[i], mappings)

def process_potential_aliased_element(element, parent, identifier, mappings):
    if type(element) is unicode and element in mappings:
        parent[identifier] = mappings[element]
    elif type(element) is dict or type(element) is list:
         replace_aliased(element, mappings)

def replace_aliased(json_doc, mappings):
    if type(json_doc) is dict:
        for key in json_doc.keys():
            process_potential_aliased_element(json_doc[key], json_doc, key, mappings)   
    elif type(json_doc) is list:
        for i in range(0, len(json_doc)):
            process_potential_aliased_element(json_doc[i], json_doc, i, mappings)
    
def post_deserialization(json_doc):
    mappings = {PREFIX + 'root' : json_doc}
    get_alias_mappings(None, None, json_doc, mappings)
    replace_aliased(json_doc, mappings)
