#!/usr/bin/python
import json
import unittest

import challenge
from structure import Structure

class TestChallenge(unittest.TestCase):

    def compare_structure(self, first, second):
        structure = Structure(first)
        structure_prime = Structure(second)
        self.assertEqual(structure, structure_prime)
    
    def obj_to_json_to_obj(self, json_doc):
        challenge.prepare_for_serialization(json_doc)
        json_string = json.dumps(json_doc)
        new_json_doc = json.loads(json_string)
        challenge.post_deserialization(new_json_doc)
        return new_json_doc

    def test_empty_dict(self):
        empty_dict = {}
        empty_dict_prime = {}

        empty_dict_prime = self.obj_to_json_to_obj(empty_dict_prime)
        self.compare_structure(empty_dict, empty_dict_prime)
        
    def test_empty_list(self):
        empty_list = []
        empty_list_prime = []

        empty_list_prime = self.obj_to_json_to_obj(empty_list_prime)
        self.compare_structure(empty_list, empty_list_prime)
    
    def test_single_element_dict(self):
        single_element_dict = {'element' : 'element'}
        single_element_dict_prime = {'element' : 'element'}

        single_element_dict_prime = self.obj_to_json_to_obj(single_element_dict_prime)
        self.compare_structure(single_element_dict, single_element_dict_prime)

    def test_single_element_cycle_dict(self):
        single_element_dict = {}
        single_element_dict['element'] = single_element_dict
        single_element_dict_prime = {}
        single_element_dict_prime ['element'] = single_element_dict_prime

        single_element_dict_prime = self.obj_to_json_to_obj(single_element_dict_prime)
        self.compare_structure(single_element_dict, single_element_dict_prime)

    def test_mixed_elements(self):
        mixed_element = json.loads('[{"a": 0},1,2,"b", [{"c": "loren ipsum","d": {}}]]')
        mixed_element[4][0]['test'] = mixed_element[4]
        mixed_element_prime = json.loads('[{"a": 0},1,2,"b", [{"c": "loren ipsum","d": {}}]]')
        mixed_element_prime[4][0]['test'] = mixed_element_prime[4]        

        mixed_element_prime = self.obj_to_json_to_obj(mixed_element_prime)
        self.compare_structure(mixed_element, mixed_element_prime)

    def test_root_cycle_dict(self):
        a = {'key0': 'val0'}
        b = {'key1': a}
        a['key2'] = b
        root_cycle = a
        a = {'key0': 'val0'}
        b = {'key1': a}
        a['key2'] = b
        root_cycle_prime = a

        root_cycle_prime = self.obj_to_json_to_obj(root_cycle_prime)
        self.compare_structure(root_cycle, root_cycle_prime)

    def test_root_cycle_list(self):
        a = {'key0': 'val0'}
        b = {'key1': a}
        a['key2'] = b
        list = [a, b]
        root_cycle = list
        a = {'key0': 'val0'}
        b = {'key1': a}
        a['key2'] = b
        list = [a, b]
        root_cycle_prime = list

        root_cycle_prime = self.obj_to_json_to_obj(root_cycle_prime)
        self.compare_structure(root_cycle, root_cycle_prime)

    def test_multiple_cycles_to_same_element(self):
        a = {'key0': 'val0'}
        b = {'key1': a}
        a['key2'] = b
        elements = []
        c = {'key3' : elements}
        d = {'key4' : 'val1'}
        elements.append(c)
        elements.append(d)
        elements.append(a)
        a['elements'] = elements
        b =[]
        b.append(a)
        multiple_cycles = b
        a = {'key0': 'val0'}
        b = {'key1': a}
        a['key2'] = b
        elements = []
        c = {'key3' : elements}
        d = {'key4' : 'val1'}
        elements.append(c)
        elements.append(d)
        elements.append(a)
        a['elements'] = elements
        b =[]
        b.append(a)
        multiple_cycles_prime = b

        multiple_cycles_prime = self.obj_to_json_to_obj(multiple_cycles_prime)
        self.compare_structure(multiple_cycles, multiple_cycles_prime)

    # Recursion exceeds the stack limit with a depth of around 485
    def test_deep_cycle(self):
        a = {}
        deep_cycle = {}
        for i in range (0,100):
            deep_cycle = {i:a}
            a = deep_cycle
        a['root'] = deep_cycle

        a = {}
        deep_cycle_prime = {}
        for i in range (0,100):
            deep_cycle_prime = {i:a}
            a = deep_cycle_prime
        a['root'] = deep_cycle_prime

        deep_cycle_prime = self.obj_to_json_to_obj(deep_cycle_prime)
        self.compare_structure(deep_cycle, deep_cycle_prime)

if __name__ == '__main__':
    unittest.main()
